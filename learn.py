
  #  Adapter le code précédent pour afficher tous les nombres de 1 à 100.
for num in range(100):
	print(num+1)

   # Afficher un rectangle de taille 4x5 composé de "x".
rectangleCell = "x"
i=0
while i< 4:
	print(rectangleCell*5)
	i+=1
   # Afficher tous les nombres de à 100 à 1.
for num in range(100,0,-1):
    print(num)

  #  Afficher un triangle composé de 10 "o" au total.             4 3 2 1
triangleCell = "O"
i=0
nbrOfCellByLine=4

while i <4:
  	print(triangleCell* nbrOfCellByLine)
  	i+=1
  	nbrOfCellByLine-=1


  # Écrire un programme qui demande 5 prénoms, les stocke dans un tableau, puis les affiche.

def askingForFirstNameXTimes(x):
	firstnameCollection = list()
	i=0
	while i < x:
  		inspirationForName=input("Gimme a name pal")
  		firstnameCollection.append(inspirationForName)
  		i+=1
  	return firstnameCollection

askingForFirstNameXTimes(5)

#Compléter ce code pour afficher les films diffusés après 12h :

horaires = [
    {'film': 'Seul sur Mars', 'heure': 9},
    {'film': 'Astérix et Obélix Mission Cléopâtre', 'heure': 10},
    {'film': 'Star Wars VII', 'heure': 15},
    {'film': 'Time Lapse', 'heure': 18},
    {'film': 'Fatal', 'heure': 20},
    {'film': 'Limitless', 'heure': 20},
]

def getMoviesBroadcastedAfterHourGiven(int, listOfMovies):
	moviesFilteredByHour= list()
	for movie in listOfMovies:
		if movie['heure'] > int:
			moviesFilteredByHour.append(movie)
	return moviesFilteredByHour

getMoviesBroadcastedAfterHourGiven(12, horaires)


